import sys

instr0 = {"nop": 0, "scf": 1, "ccf": 2, "icf": 3, "czf": 4, "ret": 5}
instr1r = {"not": 128, "inc": 136, "dec": 144, "push": 152, "pop": 160}
instr1l = {"jump": 36864, "jc": 38912, "jnc": 40960, "jz": 43008, "jnz": 45056,
           "js": 47104, "jns": 49152, "call": 51200, "ja": 53248, "je": 55296,
           "jb": 57344}
instr2r = {"src": 2048, "slc": 2560}
instr2m = {"ld": 3072, "str": 3584}
instr2adr = {"add": 4096, "adc": 8192, "sub": 12288, "sbc": 16384,
             "and": 20480, "or": 24576, "xor": 28672, "mov": 32768}
reg = {"A": 0b000, "R0": 0b001, "R1": 0b010, "R2": 0b011,
       "R3": 0b100, "R4": 0b101, "R5": 0b110, "DPTR": 0b111}

adr_modes = {"register": 0, "value": 1, "adress": 2, "variable": 3}


class myException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def convert0(line):
    command = line[0]
    if len(line) > 1:
        raise myException("ERROR too many arguments for: " + command)
    return instr0[command]


def convert1r(line):
    command = line[0]
    if len(line) < 2:
        raise myException("ERROR not enough arguments for: " + command)
    elif len(line) > 2:
        raise myException("ERROR too many arguments for: " + command)
    arg = line[1]
    if arg[0] != "%":
        raise myException("ERROR argument should be a register: " + command)
    register = arg[1:]
    if register not in reg:
        raise myException("ERROR argument should be a register: " + command)
    return instr1r[command] | reg[register]


def convert1l(line):
    command = line[0]
    if len(line) < 2:
        raise myException("ERROR not enough arguments for: " + command)
    elif len(line) > 2:
        raise myException("ERROR too many arguments for: " + command)
    arg = line[1]
    if arg[0] == "%" or arg[0] == "$" or arg[0] == "@":
        raise myException("ERROR argument should be a label: " + command)
    return instr1l[command] | 0


def convert2r(line):
    command = line[0]
    if len(line) < 3:
        raise myException("ERROR not enough arguments for: " + command)
    elif len(line) > 3:
        raise myException("ERROR too many arguments for: " + command)
    arg1 = line[1]
    arg2 = line[2]
    if arg1[0] != "%":
        raise myException("ERROR first argument should be a register: "
                          + command)
    register = arg1[1:]
    if arg2[0] != "$":
        raise myException("ERROR second argument should be a number: "
                          + command)
    if register not in reg:
        raise myException("ERROR argument should be a register: " + command)
    if not arg2[1:].isnumeric():
        raise myException("ERROR argument should be a number: " + command)
    num = int(arg2[1:])
    if num >= 2 ** 6:
        raise myException("ERROR argument is too big: " + command)
    return instr2r[command] | (register << 6) | num


def convert2m(line):
    command = line[0]
    if len(line) < 3:
        raise myException("ERROR not enough arguments for: " + command)
    elif len(line) > 3:
        raise myException("ERROR too many arguments for: " + command)
    arg1 = line[1]
    arg2 = line[2]
    if command == "ld":
        arg1 = line[2]
        arg2 = line[1]
    if arg1[0] != "%":
        raise myException("ERROR first argument should be a register: "
                          + command)
    register = arg1[1:]
    if register not in reg:
        raise myException("ERROR argument should be a register: " + command)
    if not arg2.isnumeric():
        raise myException("ERROR argument should be a memory adress: "
                          + command)
    num = int(arg2)
    if num >= 2 ** 6:
        raise myException("ERROR argument is too big: " + command)
    if num < 0:
        raise myException("ERROR argument must be non-negative: " + command)

    if command == "ld":
        return instr2r[command] | (num << 3) | register
    return instr2r[command] | (register << 6) | num


def convert2adr(line):
    command = line[0]
    if len(line) < 3:
        raise myException("ERROR not enough arguments for: " + command)
    elif len(line) > 3:
        raise myException("ERROR too many arguments for: " + command)
    arg1 = line[1]
    arg2 = line[2]
    if command == "ld":
        arg1 = line[2]
        arg2 = line[1]
    if arg1[0] != "%":
        raise myException("ERROR first argument should be a register: "
                          + command)
    register = arg1[1:]
    if register not in reg:
        raise myException("ERROR argument should be a register: " + command)

    if arg2[0] == "%":
        register2 = arg2[1:]
        if register2 not in reg:
            raise myException("ERROR argument should be a register: "
                              + command)
        return (instr2adr[command] | (reg[register] << 9) |
               (adr_modes["register"] << 7) | reg[register2] << 4)

    elif arg2[0] == "$":
        if not arg2[1:].isnumeric():
            raise myException("ERROR argument should be a memory adress: "
                              + command)
        num = int(arg2[1:])
        if num >= 2 ** 7:
            raise myException("ERROR argument is too big: " + command)
        return (instr2adr[command] | (reg[register] << 9) |
                (adr_modes["value"] << 7) | num)

    elif arg2[0] == "@":
        register2 = arg2[1:]
        if register2 not in reg:
            raise myException("ERROR argument should be a register: "
                              + command)
        return (instr2adr[command] | (reg[register] << 9) |
               (adr_modes["adress"] << 7) | reg[register2] << 4)

    elif arg2.isnumeric():
        num = int(arg2)
        if num >= 2 ** 6:
            raise myException("ERROR argument is too big: " + command)
        if num < 0:
            raise myException("ERROR memory adress must be non-negative: "
                              + command)
        return (instr2adr[command] | (reg[register] << 9) |
               (adr_modes["variable"] << 7) | num)


instr_list = [(instr0, convert0), (instr1r, convert1r), (instr1l, convert1l),
              (instr2r, convert2r), (instr2m, convert2m),
              (instr2adr, convert2adr)]


def main(argv):
    try:
        if len(argv) < 2:
            raise myException("ERROR No program file is specified")
        name = argv[1]
        source = open(name, 'r')
        output = open(name[:name.find(".")] + ".bin", 'wb')
        for line in source:
            line = line[:line.find(" ;")]
            line = line.split()
            if len(line) >= 1:
                found = False
                command = line[0]
                for instr_type in instr_list:
                    if command in instr_type[0]:
                        found = True
                        converted = instr_type[1](line)
                        byte_list = bytes([converted // 256, converted % 256])
                        output.write(byte_list)
                if found is False:
                    raise myException("ERROR unknown command: " + command)
    except myException as e:
        print(e.value)

if __name__ == "__main__":
    main(sys.argv)
